import logo from './logo.svg';
import './App.css';
import Greetings from './components/Greetings.js';

function App() {
  let name = "M.Fatih Candan";
  
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Greetings name={name}/>
      </header>
    </div>
  );
}

export default App;
